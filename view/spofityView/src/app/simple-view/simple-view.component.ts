import {Component, OnInit} from '@angular/core';
import {Item} from './PlayList';
import {SimpleViewService} from './simple-view.service';
import {User} from "./User";

@Component({
    selector: 'app-simple-view',
    templateUrl: './simple-view.component.html',
    styleUrls: ['./simple-view.component.css']
})
export class SimpleViewComponent implements OnInit {
    item: Item;
    status: any;
    user: User;

    constructor(private simpleViewService: SimpleViewService) {
    }

    ngOnInit(): void {
    }

    getAllPlaylists = (): any => {
        fetch('http://localhost:8080/api/v1/top-artists')
            .then(value => value.json())
            .then(data => {
                console.log(data);
            });
    };

    loginOn = (): any => {
        fetch('http://localhost:8080/api/v1/login')
            .then(value => value.text())
            .then(value => window.location.replace(value))
            .catch(reason => console.log(reason));
    };

    // getMyInfo = (): any => {
    //     fetch('http://localhost:8080/api/v1/user-details')
    //         .then(value => value.text())
    //         .then(value => this.user = value)
    //         .catch(reason => console.log(reason));
    // };

    click() {
        this.simpleViewService.click().subscribe(
            value => {
                console.log(value);
                this.user = value;
            },
            error => console.log(error),
            () => console.log("asdasdasd")
        )
    }
}

