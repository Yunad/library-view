import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from "rxjs";

const PLAYLIST_URL = environment.playlistUrl;
const LOGIN_URL = environment.loginUrl;
const USER_DETAILS = environment.userDetails;
const headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
});

@Injectable({
    providedIn: 'root'
})
export class SimpleViewService {

    constructor(private httpClient: HttpClient) {
    }

    click(): Observable<any> {
        return this.httpClient.get(USER_DETAILS);
    }
}
