import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {SimpleViewComponent} from './simple-view/simple-view.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';

@NgModule({
    declarations: [
        AppComponent,
        SimpleViewComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
    ],
    providers: [HttpClient],
    bootstrap: [AppComponent]
})
export class AppModule {
}
